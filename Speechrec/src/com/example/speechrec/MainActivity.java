package com.example.speechrec;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity 
{

    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Intent i= new Intent(getApplicationContext(), MyService.class);
		// potentially add data to the intent
		i.putExtra("KEY1", "Value to be used by the service");
		getApplicationContext().startService(i);
        
    }
}


